package Constants;

public interface constants {
    String homePageTitle = "My Store";
    String contactUS = "Contact us";
    String signIn = "Sign in";
    String women = "Women";
    String dresses = "Dresses";
    String tshirts = "T-Shirts";
    String popular = "POPULAR";
    String bestSellers = "BEST SELLERS";
    String callUsNow = "Call us now: 0123-456-789";
    String search = "Search";

}
