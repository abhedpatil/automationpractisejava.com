package Data;

import org.testng.annotations.DataProvider;

public class HomePageData {

    //This class is mainly to provide TestNG Data providers if being used or create a JSON File and pass data.
    @DataProvider(name = "search") //DataSet
    public Object[][] searchData() {
        return new Object[][]{
                {"t-shirts"},
                {"jeans"},
        };
    }
}
