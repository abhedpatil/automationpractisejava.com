package Util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.testng.Assert.fail;

public abstract class AbstractPage {

    public final static int TIMEOUT = 60;// in seconds
    private final static byte longerWait = 5; // in seconds
    private final static int millisecondsToSleep = 1000;
    public WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public static String getFormatedDate(String format) {
        Date date = new Date();
        return new SimpleDateFormat(format).format(date);
    }

    // Retrieves the current date as ISO 8601 string
    public static String getISOdate() {
        return getFormatedDate("yyyy-MM-dd");
    }

    // Retrieves the current time as ISO 8601 string
    public static String getISOtime() {
        return getFormatedDate("HH:mm:ss");
    }

    public void waitForObjectVisible(WebElement element) {
        try {
            for (int second = 0; ; second++) {
                if (second > TIMEOUT) {
                    fail("It took longer than " + TIMEOUT + " seconds in the attempt to wait for object '" + element.getText() + "'!");
                }
                if (element.isDisplayed()) {
                    return;
                }
                sleep(millisecondsToSleep);
            }
        } catch (Exception e) {
            e.getStackTrace();
            e.getMessage();
        }
    }

    public void log(String string) {

        String time = getFormatedDate(getISOdate() + " " + getISOtime());
        System.out.println("** " + time + " " + string);
    }

    public void sleep(int milliseconds) {
        try {
            if (milliseconds > 1000) {
                log("Waiting for " + (milliseconds / 1000) + " second(s) before next interaction...");
            }
            Thread.sleep(milliseconds);
        } catch (Exception e) {
            System.out.println("Interrupt Exception Encountered.");
        }
    }
}
