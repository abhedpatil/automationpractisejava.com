package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterMethod;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    public static WebDriver driver = null;

    public WebDriver initializeDriver() {

        try {
            Properties prop = new Properties();
            FileInputStream file = new FileInputStream("D:\\Automation\\Projects\\automationpractisejava.com\\src\\test\\resources\\setup.properties");
            prop.load(file);
            String browserName = prop.getProperty("browser");
            String url = prop.getProperty("url");

            if (browserName.equals("chrome")) {
                System.setProperty("webdriver.chrome.driver", "D:\\Automation\\Projects\\automationpractisejava.com\\chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.get(url);
                driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
            } else if (browserName.equals("IE")) {
                //launch IE Driver
                //  System.setProperty("webdriver.chrome.driver", "D:\\Automation\\Projects\\automationpractisejava.com\\IE.exe");
                driver = new InternetExplorerDriver();
                driver.manage().window().maximize();
                driver.get(url);
                driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
            }
        } catch (IOException e) {
            e.getStackTrace();
            e.getMessage();
        }
        return driver;
    }

    @AfterMethod
    public void teardown() {
        driver.quit();
    }
}
