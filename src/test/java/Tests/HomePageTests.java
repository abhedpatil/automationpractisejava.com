package Tests;

import Pages.HomePage;
import org.testng.annotations.Test;

public class HomePageTests extends BaseTest {


    @Test(description = "Validate all Home Page Objects, story# 123")
    public void validateAllObjectsAreDisplayedOnHomePage() {
        driver = initializeDriver();
        HomePage homePage = new HomePage(driver);

        homePage.validateUserNavigatedToHomePageAndAllObjectsAreDisplayed();
    }
}
