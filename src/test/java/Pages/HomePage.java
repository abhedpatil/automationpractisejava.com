package Pages;

import Constants.constants;
import Util.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePage extends AbstractPage implements constants {

    //public WebDriver driver;

    //PageObjectDeclaration
    @FindBy(css = "#contact-link")
    WebElement contactUs;
    @FindBy(className = "login")
    WebElement signIn;
    @FindBy(className = "shop-phone")
    WebElement callUs;
    @FindBy(id = "search_query_top")
    WebElement search;
    @FindBy(id = "header_logo")
    WebElement yourLogo;
    @FindBy(xpath = "//a[@class='homefeatured']")
    WebElement popular;
    @FindBy(xpath = "//a[@class='blockbestsellers']")
    WebElement bestSellers;


    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void validateUserNavigatedToHomePageAndAllObjectsAreDisplayed() {
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, constants.homePageTitle);
        log("Verified titles " + actualTitle);

        //wait for all objects to be displayed
        waitForObjectVisible(contactUs);
        waitForObjectVisible(signIn);
        waitForObjectVisible(callUs);

        //valdiate objects and text within objects
        Assert.assertTrue(contactUs.isDisplayed(), "Object not displayed (Element: contactUs) ");
        log("Verified object displayed " + contactUs);
        Assert.assertEquals(contactUs.getText(), constants.contactUS);
        log("Verified Contact Us text " + contactUs.getText());

        Assert.assertTrue(signIn.isDisplayed(), "\"Object not displayed (Element: signIn) ");
        log("Verified object displayed " + signIn);
        Assert.assertEquals(signIn.getText(), constants.signIn);
        log("Verified Contact Us text " + signIn.getText());

        Assert.assertTrue(callUs.isDisplayed(), "\"Object not displayed (Element: signIn) ");
        log("Verified object displayed " + callUs);
        Assert.assertEquals(callUs.getText(), constants.callUsNow);
        log("Verified Contact Us text " + callUs.getText());

        Assert.assertTrue(yourLogo.isDisplayed(), "\"Object not displayed (Element: yourLogo) ");
        log("Verified object displayed " + yourLogo);

        Assert.assertTrue(search.isDisplayed(), "\"Object not displayed (Element: search) ");
        log("Verified object displayed " + search);

        Assert.assertTrue(popular.isDisplayed(), "\"Object not displayed (Element: popular) ");
        log("Verified object displayed " + popular);
        Assert.assertEquals(popular.getText(), constants.popular);
        log("Verified Contact Us text " + popular.getText());

        Assert.assertTrue(bestSellers.isDisplayed(), "\"Object not displayed (Element: bestSellers) ");
        log("Verified object displayed " + bestSellers);
        Assert.assertEquals(bestSellers.getText(), constants.bestSellers);
        log("Verified Contact Us text " + bestSellers.getText());
    }
}
